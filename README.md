# Réponses aux questions de l'examen

## Question 1 :
	* On constate que les codes postaux sont de types int
	* Le type de retour doit etre un Integer
	
## Question 2 :
	* La liste contient *36482* éléments
	* On voit bien que le nombre d"'éléments est bien inférieur au nombre de lignes du fichier
	* On conclut donc qu'il y a bien des doublons dans  le fichier
 
## Question 3 :
	* Le 62 est le departement qui compte le plus de commune
	
## Question 5 :
	* Anjou est associé au plus grand nombre
	
## Question 6 :
	* Le predicat devient une BiFunction
	
	