package part01;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fileName = "./files/laposte_hexasmal.csv";
		
		Function<String, Integer> toPostalCode = s -> Integer.parseInt(s.split(";")[2]);
		Function<String, String> toName = s -> s.split(";")[3];
		
		Path path = Paths.get(fileName);
		List<Integer> ListOfCodePostal = new ArrayList<>();;
		try (Stream<String> lines = Files.lines(path)) {
			ListOfCodePostal = lines.skip(1)
						 .map(toPostalCode)
						 .collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		//System.out.println(ListOfCodePostal);
		
		//Second Stream
		List<Commune> communes = new ArrayList<>();
		try (Stream<String> lines = Files.lines(path)) {
			communes = lines.skip(1)
							.map(s -> new Commune(toPostalCode.apply(s.toString()), toName.apply(s.toString())))
							.distinct()
							.collect(Collectors.toList());
				 
			//communes.add(new Commune(toPostalCode.apply(lines.toString()), toName.apply(lines.toString())));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		//System.out.println(communes);
		//System.out.println(communes.size());
		
		
		Function<Commune, Integer> toDepartementCode = commune -> commune.getCodePostal()/1000;
		
		//First Map for Departements
		Map<Integer, List<Commune>> departements =  new HashMap<Integer, List<Commune>>();
		
		departements = communes.stream()
	            			   .collect(Collectors.groupingBy(toDepartementCode));
		
		System.out.println("================== Departements Map =================== ");
		departements.forEach((key, value) -> System.out.println(key + " | " + value));
		
		
		//Second Map
		Map<Integer, Long> histogram =  new HashMap<Integer, Long>();
		histogram = communes.stream()
				            .collect(Collectors.groupingBy(toDepartementCode, Collectors.counting()));
				          
		System.out.println("================== Second Map =================== ");
		histogram.forEach((key, value) -> System.out.println(key + " | " + value));
		
		System.out.println("Il y a : " + departements.size() + " Départements");
		
		Long code = communes.stream()
        					.collect(Collectors.groupingBy(toDepartementCode, Collectors.counting()))
			        		.values().stream()
			        				 .mapToLong(s->s)
			        				 .max().getAsLong();
		System.out.println("nbre de commune max : " + code);

	}

}
