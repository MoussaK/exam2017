package part03;

import java.util.ArrayList;
import java.util.List;

public class SubwayAnalyzer {
	public Station createStation(String line) {
		String label = line.split(";")[2];
		Station station = new Station(label, Long.parseLong(line.split(";")[3]));
		List<Object> list = new ArrayList<>();
		list.add(Line.of(label));
		station.setLine(list);
		
		return station;
	}
}
