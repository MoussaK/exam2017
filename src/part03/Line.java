package part03;

import java.util.List;

public class Line {
	
	private String label;
	private List<Object> station;
	/**
	 * @param label
	 * @param station
	 */
	public Line(String label, List<Object> station) {
		super();
		this.label = label;
		this.station = station;
	}
	public Line(String label) {
		super();
		this.label = label;
	}
	public Line() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the station
	 */
	public List<Object> getStation() {
		return station;
	}
	/**
	 * @param station the station to set
	 */
	public void setStation(List<Object> station) {
		this.station = station;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Line [label=");
		builder.append(label);
		builder.append(", station=");
		builder.append(station);
		builder.append("]");
		return builder.toString();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Line))
			return false;
		Line other = (Line) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		return true;
	}
	
	
	/*Méthode static, appartient à la classe plutôt qu'à une instance de la classe*/
	public static Line of(String label) {
		return new Line(label);
	}
	
}
