package part03;

import java.util.List;

public class Station {
	private String name;
	private Long trafic;
	private List<Object> Line;
	/**
	 * @param name
	 * @param trafic
	 * @param line
	 */
	public Station(String name, Long trafic, List<Object> line) {
		super();
		this.name = name;
		this.trafic = trafic;
		Line = line;
	}
	public Station(String name, long trafic) {
		super();
		this.name = name;
		this.trafic = trafic;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the trafic
	 */
	public Long getTrafic() {
		return trafic;
	}
	/**
	 * @param trafic the trafic to set
	 */
	public void setTrafic(Long trafic) {
		this.trafic = trafic;
	}
	/**
	 * @return the line
	 */
	public List<Object> getLine() {
		return Line;
	}
	/**
	 * @param line the line to set
	 */
	public void setLine(List<Object> line) {
		Line = line;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Station [name=");
		builder.append(name);
		builder.append(", trafic=");
		builder.append(trafic);
		builder.append(", Line=");
		builder.append(Line);
		builder.append("]");
		return builder.toString();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Station))
			return false;
		Station other = (Station) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
