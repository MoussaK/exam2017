package part02;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fileName = "./files/laposte_hexasmal.csv";
		
		//Function<String, Integer> toPostalCode = s -> Integer.parseInt(s.split(";")[2]);
		Function<String, String> toName = s -> s.split(";")[3];
		Predicate<String> starsWithEn = communeName -> communeName.toUpperCase()
																  .contains(" EN ");
		Function<String, String> cityAfterEN = communeName -> communeName.substring(
																		  communeName.indexOf("EN ") + 2
																		 );
		Function<String, String> cityBeforeEN = communeName -> communeName.substring(
																		  0,
				  														  communeName.indexOf("EN ")
				 														 );
		Path path = Paths.get(fileName);
		List<String> communeNames = new ArrayList<>();;
		try (Stream<String> lines = Files.lines(path)) {
			communeNames = lines.skip(1)
						 		.map(toName)
						 		.collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		System.out.println(communeNames);
		
		Map<String, List<String>> histogram =  new HashMap<String, List<String>>();
		histogram = communeNames.stream()
								.filter(starsWithEn)
				            	.collect(Collectors.groupingBy(
				            						cityAfterEN,
				            						Collectors.mapping(cityBeforeEN,
				            										   Collectors.toList())));
				          
		System.out.println("================== Third Map =================== \n");
		histogram.forEach((key, value) -> System.out.println(key + " ==> " + value));
		
		/*To get the key corresponding to the max value in the Map*/
		BiFunction<Map<String, Long>, Long, String> keyofMapMaxValue = (map, maxValue) -> {
			Set<Entry<String, Long>> entries = map.entrySet();
			for (Entry<String, Long> entry : entries) {
				if(entry.getValue() == maxValue)
					return entry.getKey();
			}
			return "";
		};
		
		Map<String, Long> tmp = communeNames.stream()
											.filter(starsWithEn)
							            	.collect(Collectors.groupingBy(
							            						cityAfterEN,
							            						Collectors.counting()));
		System.out.println(tmp);
		Long val = tmp.values().stream()
     		   			 		 .mapToLong(s -> s)
     		   					 .max().getAsLong();
		System.out.println("\nEndroit assoicié au plus grand nombre de comunes : " + keyofMapMaxValue.apply(tmp, val));
	
		BiFunction<String, String, Boolean> starsWithWord = (communeName, word) -> communeName.toUpperCase()
				  												  							  .contains(" " + word + " ");
		BiFunction<String, String, String> cityAfterWord = (communeName, word) -> communeName.substring(
				  																	communeName.indexOf(word + " ") + word.length() - 1
				 																  );
		BiFunction<String, String, String> cityBeforeWord = (communeName, word) -> communeName.substring(
																		   0,
																		   communeName.indexOf(word + " ")
																		  );
		/*Map<String, List<String>> CommunesSous =  new HashMap<String, List<String>>();
		CommunesSous = communeNames.stream()
								   .filter(starsWithEn)
				            	   .collect(Collectors.groupingBy(
				            						  cityAfterWord,
				            						  Collectors.mapping(cityBeforeWord,
				            										   Collectors.toList())));
				          
		System.out.println("================== Second Map =================== \n");
		histogram.forEach((key, value) -> System.out.println(key + " ==> " + value));*/
	}

}
